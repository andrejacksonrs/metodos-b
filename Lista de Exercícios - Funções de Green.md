# Lista de Exercícios - Funções de Green

> *Baseada nas vídeo-aulas de Métodos de Física Teórica B criados por **Gildemar Carneiro** durante o Semestre Letivo Suplementar (SLS) da UFBA (ocorrido entre Setembro de 2020 a dezembro de 2020)*
>
> *Link do canal do Youtube em que as vídeo-aulas de Gildemar foram publicadas: https://www.youtube.com/channel/UCz3bohPUbxg07ceMli9GZ6w/videos*
>
> *Lista elaborada pelo estudante: André Jackson Ramos Simões*

## Vídeo 21

### Seção 1

**(1)** Resolva a seguinte equação diferencial, mostre que a solução geral depende de uma constante arbitrária $A$, e pode ser escrita como:

​													$m\dot{v} + bv = 0$				;				 $v(t) = A.e^{(\frac{-b}{m})t}$

**(2)** Uma partícula de massa m está inicialmente em repouso em um determinado referencial de modo que sua velocidade $v(t=0) = v(0) = 0$. Em um instante $\tau$ essa partícula sofre a ação de uma força instantânea dada por uma delta de Dirac de modo que a sua equação de movimento é: 
$$
m\dot{v} + bv = F(t) = I.\delta(t - \tau)
$$

Determine a constante $A$, e por fim $v(t)$ da partícula em função do Impulso $I$ que é:
$$
I = \int_{\tau-\epsilon}^{\tau+\epsilon} F \,dt \,\,\,\,\,\, ; \,\,\,\,\,\,  \epsilon \rightarrow 0
$$

**(3)** Generalize o resultado anterior mostrando que caso a força $F(t)$ seja contínua, e entendida como uma soma de infinitas deltas de Dirac, a velocidade é dada pela seguinte integral:
$$
v(t) = \int_{\tau_0}^{t}\frac{e^{(\frac{-b}{m})(t-\tau)}}{m} F(\tau) \,d\tau
$$

**(4)** Verifique que a velocidade dada acima satisfaz a equação de movimento:
$$
m\dot{v} + bv = 0
$$
Sabendo que para derivar uma integral com limites de integração variáveis tem-se a seguinte equação:
$$
f(t) = \int_{\alpha(t)}^{\beta(t)}G(\tau,t)\,d\tau \implies \frac{df}{dt} = \frac{d\beta}{dt}G(\beta,t) - \frac{d\alpha}{dt}G(\alpha,t) + \int_{\alpha(t)}^{\beta(t)}\frac{\partial G(\tau,t)}{\partial t}\,d\tau
$$

### Seção 2

**(5)** Resolver a seguinte equação diferencial homogênea no caso de amortecimento sub-crítico:

$$
\ddot{x} + 2\lambda\dot{x} + \omega_0^2x = 0
$$

**(6)** Resolver a seguinte equação usando o método da função de Green, para F sendo uma delta de Dirac:

$$
\ddot{x} + 2\lambda\dot{x} + \omega_0^2x = f = \frac{F}{m}
$$

**(7)** Resolver a seguinte equação usando o método da função de Green, para F sendo função contínua mas que pode ser entendida como uma sequência de deltas de Dirac, como na seção 1:

$$
\ddot{x} + 2\lambda\dot{x} + \omega_0^2x = f = \frac{F}{m}
$$

## Vídeo 22	

**(8)** Desenvolver a teoria da função de Green conforme Riley, nas páginas 511 a 513 (3ª Edição Cambridge - Mathematical Methods for Physics and Engineering) e concluir os itens (i) (ii) e (iii), especialmente a propriedade de descontinuidade/continuidade das derivadas.

**(9)** Encontre a função de Green para o operador de Sturm-Liouville conforme Butkov na seção 12.2 (Mathematical Physics - Addison Wesley First Printing 1973)

## Vídeo 23

**(10)** Resolver o problema da corda distendida (Butkov pg. 506 - Exemplo 3) aplicando a teoria vista no vídeo 22, com o G> e o G<;

**(11)** Mostre que colocando a seguinte equação na forma de Sturm-Liouville, não é possível aplicar o formalismo da função de Green para resolver o problema, com as condições iniciais:
$$
\ddot{x} + 2\lambda\dot{x} + \omega_0^2x = f = \frac{F}{m} \,\,\,\,\,\,\,;\,\,\,\, \dot{x}(0) = x(0) = 0
$$

**(12)** Resolva a seguinte equação com o método da função de Green aplicando a teoria da teoria da função de Green para equações do tipo Sturm-Liouville (Exemplo do Rilley pg. 570):
$$
y'' + \frac{y}{4} = sen(2x) \,\,\,\,\,\,\,\ ;\,\,\,\,\,\,\,\,y(0)=y(\pi) = 0
$$

## Vídeo 24

**(13)** Encontre a expansão em série da função de Green correspondente a solução da equação:

$$
y'' + \frac{y}{4} = sen(2x) \,\,\,\,\,\,\,\ ;\,\,\,\,\,\,\,\,y(0)=y(\pi) = 0
$$

**(14)** Encontre a expansão em série da função de Green correspondente a solução da equação:

$$
y'' + \frac{y}{4} = \frac{x}{2} \,\,\,\,\,\,\,\ ;\,\,\,\,\,\,\,\,y(0)=y(\pi) = 0
$$

## Vídeo 25

**(15)** Mostre a equivalência abaixo, e resolva a equação da Difusão do Calor via função de Green:
$$
\frac{\partial u}{\partial t} = \alpha^2\frac{\partial^2 u}{\partial x^2} \,\,\,\,\, ; \,\,\,\,\, u(x,0) = f(x) \,\, \leftrightarrow \,\, g(x|\xi,0) = \delta(x-\xi)
$$
Gabarito:
$$
u(x,t) = \int_{-\infin}^{+\infin} \frac{1}{\sqrt{4\pi\alpha^2 t}} \, exp\left[{\frac{-(\xi-x)^2}{4\alpha^2 t}}\right] \, f(\xi) \, d\xi
$$
**(16)** Verifique, derivando $u(x,t)$, que a solução encontrada no problema anterior de fato satisfaz a equação da Difusão do Calor.



## Vídeo 26

**(17)** Considere a função f cuja paridade é dada por:
$$
f(-x) = sf(x) \,\,;\, com \,\,\, s^2 = 1
$$
Verifique que a paridade da função de Green do problema anterior de difusão em uma barra infinita é igual a paridade da função f.

**(18)** Resolva a equação do calor para uma barra semi infinita para $t>t_0$ (exemplo pg. 531 do Butkov):
$$
\frac{\partial u}{\partial t} = \alpha^2\frac{\partial^2 u}{\partial x^2} \,\,\,\,\, ; \,\,\,\,\, u(x,0) = 0 \,\,\,\,\, ; \,\,\,\,\, u(x,t_0) = f_0
$$


Gabarito: 
$$
u(x,t) = f_0 .erf\left[\frac{x}{2a\sqrt{(t - t_0)}}\right]
$$


# Observações

- A função de Green é representada por G (g maiúsculo) quando a não homogeneidade está na equação. Por outro lado, a função de Green é representada por g (g minúsculo) quando a não homogeneidade está nas condições de contorno/iniciais do problema.



## Exercícios de Provas anteriores (Referência 4)

**(19)** Usando o método da função de Green, resolva a seguinte equação:
$$
\frac{d^2y}{dx^2} = x^2
$$
Com:
$$
y(0) = 1 \,\,\,\,\,\,\,\,\,\, y(1)=2
$$


**(20)** Pelo processo de expansão da função de Green em autofunções, resolva a equação
$$
\frac{d^2y}{dx^2} + 3y = sen(2x)
$$
Com as seguintes condições de contorno: 
$$
y(0) = y(\pi) = 0
$$


**(21)** Usando a função de Green, sem expansão em série, ache a solução da equação diferencial:
$$
\frac{d^2 u}{dx^2} + k^2 u = 1
$$
Com as seguintes condições de contorno: 
$$
u(0) = 0\,\,\,\,\, \,\,\,\,\, \frac{du}{dx}\bigg|_{x=1} = 0
$$


**(22)** Usando a função de Green, com expansão em série, ache a solução da equação diferencial:
$$
\frac{d^2 u}{dx^2} + k^2 u = 1
$$
Com as seguintes condições de contorno: 
$$
u(0) = 0\,\,\,\,\, \,\,\,\,\, \frac{du}{dx}\bigg|_{x=1} = 0
$$






# Referências Bibliográficas

1. BUTKOV, Eugene. Mathematical Physics - Addison Wesley First Printing 1973.
2. RILEY, K. F. - Mathematical Methods for Physics and Engineering. 3ª Edição Cambridge.
3. ARFKEN, George B. - Mathematical methods for Physicists, a comprehensive guide. 7ª Edição, Elsevier.
4. Provas anteriores do curso de Métodos de Física Teórica B com Gildemar Carneiro localizadas no Blog Professor Não, Gildemar ! http://professornao-gildemar.blogspot.com/p/metodos-em-fisica-teorica-1.html

